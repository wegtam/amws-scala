/*
 * Copyright (c) 2017 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.amws.common

import enumeratum.values._

/**
  * Amazon marketplaces.
  *
  * @param value The string representation of the parameter value of the marketplace that must be used in the query string.
  */
sealed abstract class MarketPlace(val value: ParameterValue) extends StringEnumEntry with Product with Serializable {

  /**
    * Return the region of this marketplace.
    *
    * @return An Amazon region.
    */
  def region: Region

}

object MarketPlace extends StringEnum[MarketPlace] with CatsValueEnum[ParameterValue, MarketPlace] {

  val values = findValues

  case object CA extends MarketPlace(value = "A2EUQ1WTGCTBG2") {
    override def region: Region = Region.NorthAmerica
  }

  case object MX extends MarketPlace(value = "A1AM78C64UM0Y8") {
    override def region: Region = Region.NorthAmerica
  }

  case object US extends MarketPlace(value = "ATVPDKIKX0DER") {
    override def region: Region = Region.NorthAmerica
  }

  case object BR extends MarketPlace(value = "A2Q3Y263D00KWC") {
    override def region: Region = Region.Brazil
  }

  case object DE extends MarketPlace(value = "A1PA6795UKMFR9") {
    override def region: Region = Region.Europe
  }

  case object ES extends MarketPlace(value = "A1RKKUPIHCS9HS") {
    override def region: Region = Region.Europe
  }

  case object FR extends MarketPlace(value = "A13V1IB3VIYZZH") {
    override def region: Region = Region.Europe
  }

  case object IT extends MarketPlace(value = "APJ6JRA9NG5V4") {
    override def region: Region = Region.Europe
  }

  case object UK extends MarketPlace(value = "A1F83G8C2ARO7P") {
    override def region: Region = Region.Europe
  }

  case object IN extends MarketPlace(value = "A21TJRUUN4KGV") {
    override def region: Region = Region.India
  }

  case object JP extends MarketPlace(value = "A1VC38T7YXB528") {
    override def region: Region = Region.Japan
  }

  case object CN extends MarketPlace(value = "AAHKV2X7AFYLW") {
    override def region: Region = Region.China
  }
}

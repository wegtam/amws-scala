/*
 * Copyright (c) 2017 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.amws.common

import cats.syntax.eq._
import enumeratum.values._

import scala.collection.immutable._

/**
  * Amazon regions.
  *
  * @param value The uri of the endpoint that must be used for API access.
  */
sealed abstract class Region(val value: EndpointURI)
    extends StringEnumEntry
    with AllowAlias
    with Product
    with Serializable { self =>

  /**
    * A list of all marketplaces associated with the region.
    *
    * @return A list of all marketplaces within this region.
    */
  def marketplaces: Seq[MarketPlace] = MarketPlace.values.filter(_.region === self)

}

object Region extends StringEnum[Region] with CatsValueEnum[EndpointURI, Region] {

  val values = findValues

  case object NorthAmerica extends Region(value = "https://mws.amazonservices.com")

  case object Brazil extends Region(value = "https://mws.amazonservices.com")

  case object Europe extends Region(value = "https://mws-eu.amazonservices.com")

  case object India extends Region(value = "https://mws.amazonservices.in")

  case object China extends Region(value = "https://mws.amazonservices.com.cn")

  case object Japan extends Region(value = "https://mws.amazonservices.jp")

}

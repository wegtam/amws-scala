/*
 * Copyright (c) 2017 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.amws.reports

import com.wegtam.amws.common.ParameterValue
import enumeratum.values._

/**
  * The Schedule enumeration provides the units of time that indicate how
  * often a report request can be requested. For example, the
  * ManageReportSchedule operation uses the Schedule value to indicate how
  * often a report request is submitted.
  *
  * @see http://docs.developer.amazonservices.com/en_US/reports/Reports_Schedule.html
  *
  * @param value The string representation of the parameter value of the schedule type that must be used in the query string.
  */
sealed abstract class ScheduleType(val value: ParameterValue) extends StringEnumEntry with Product with Serializable

object ScheduleType extends StringEnum[ScheduleType] with CatsValueEnum[ParameterValue, ScheduleType] {

  val values = findValues

  /**
    * Can be used to delete a previously created report request schedule.
    */
  case object Never extends ScheduleType(value = "_NEVER_")

  case object Every15Minutes extends ScheduleType(value = "_15_MINUTES_")

  case object Every30Minutes extends ScheduleType(value = "_30_MINUTES_")

  case object EveryHour extends ScheduleType(value = "_1_HOUR_")

  case object Every2Hours extends ScheduleType(value = "_2_HOURS_")

  case object Every4Hours extends ScheduleType(value = "_4_HOURS_")

  case object Every8Hours extends ScheduleType(value = "_8_HOURS_")

  case object Every12Hours extends ScheduleType(value = "_12_HOURS_")

  case object EveryDay extends ScheduleType(value = "_1_DAY_")

  case object Every2Days extends ScheduleType(value = "_2_DAYS_")

  case object Every3Days extends ScheduleType(value = "_72_HOURS_")

  case object EveryWeek extends ScheduleType(value = "_1_WEEK_")

  case object Every14Days extends ScheduleType(value = "_14_DAYS_")

  case object Every15Days extends ScheduleType(value = "_15_DAYS_")

  case object Every30Days extends ScheduleType(value = "_30_DAYS_")

}

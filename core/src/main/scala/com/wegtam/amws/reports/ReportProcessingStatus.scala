/*
 * Copyright (c) 2017 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.amws.reports

import cats.syntax.eq._
import com.wegtam.amws.common.ParameterValue
import enumeratum.values._

/**
  * A report has a processing status.
  *
  * @param value The string representation of the parameter value of the processing status that must be used in the query string.
  */
sealed abstract class ReportProcessingStatus(val value: ParameterValue)
    extends StringEnumEntry
    with Product
    with Serializable

object ReportProcessingStatus
    extends StringEnum[ReportProcessingStatus]
    with CatsValueEnum[ParameterValue, ReportProcessingStatus] {

  val values = findValues

  /**
    * Return the processing status described by the given parameter value.
    *
    * @param v A string representation of a processing status.
    * @return An option to the processing status.
    */
  def fromParameterValue(v: ParameterValue): Option[ReportProcessingStatus] = values.find(_.value === v)

  /**
    * The report has been submitted but no further processing has been done.
    */
  case object Submitted extends ReportProcessingStatus(value = "_SUBMITTED_")

  /**
    * The report is currently being processed.
    */
  case object InProgress extends ReportProcessingStatus(value = "_IN_PROGRESS_")

  /**
    * The processing of the report has been cancelled.
    */
  case object Cancelled extends ReportProcessingStatus(value = "_CANCELLED_")

  /**
    * The processing is done and the report is ready to be fetched.
    */
  case object Done extends ReportProcessingStatus(value = "_DONE_")

  /**
    * The processing is done but the report contains no data.
    */
  case object DoneNoData extends ReportProcessingStatus(value = "_DONE_NO_DATA_")

}
